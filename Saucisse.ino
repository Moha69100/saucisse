#include <StringSplitter.h>
#include <SoftwareSerial.h>
#include <aREST.h>
#include <KeyboardAzertyFr.h>

// Create aREST instance
aREST rest = aREST();

SoftwareSerial mavoieserie(9, 8); 

int runCommand(String command);

void setup(void)
{
 
  
  Serial.begin(9600);
  
  // Start Serial
  mavoieserie.begin(9600);

  // Keyboard
  rest.function("runCommand",runCommand);

  // Give name and ID to device (ID should be 6 characters long)
  rest.set_id("SC1416");
  rest.set_name("SAUCISSE_1416");
}

void loop() { KeyboardAzertyFr.begin();
  // Handle REST calls
  rest.handle(mavoieserie); 
   KeyboardAzertyFr.end();
}

// runCommand?command=KEY_WIN|KEY_DOWN|KEY_UP|r
int runCommand(String command) {
  Serial.println(command);

  StringSplitter *splitter = new StringSplitter(command, '|', 10);
 
  for(int i = 0; i < splitter->getItemCount(); i++){
    String stringKey = splitter->getItemAtIndex(i);
    KeyboardAzertyFr.press(getKey(stringKey));
  }
  KeyboardAzertyFr.releaseAll();
  return 1;
}

uint8_t getKey(String stringKey)
{
    if(stringKey.startsWith("KEY_")){
        return getDefineKey(stringKey);
    }

    return (uint8_t*)stringKey[0];
}


uint8_t getDefineKey(String stringKey)
{
   if(stringKey == "KEY_LEFT_CTRL"){
      return KEY_LEFT_CTRL;
    } else if(stringKey == "KEY_LEFT_SHIFT"){
      return KEY_LEFT_SHIFT;
    }else if(stringKey == "KEY_LEFT_ALT"){
      return KEY_LEFT_ALT;
    }else if(stringKey == "KEY_LEFT_GUI"){
      return KEY_LEFT_GUI;
    }else if(stringKey == "KEY_RIGHT_CTRL"){
      return KEY_RIGHT_CTRL;
    }else if(stringKey == "KEY_RIGHT_SHIFT"){
      return KEY_RIGHT_SHIFT;
    }else if(stringKey == "KEY_RIGHT_ALT"){
      return KEY_RIGHT_ALT;
    }else if(stringKey == "KEY_RIGHT_GUI"){
      return KEY_RIGHT_GUI;
    }else if(stringKey == "KEY_UP_ARROW"){
      return KEY_UP_ARROW;
    }else if(stringKey == "KEY_DOWN_ARROW"){
      return KEY_DOWN_ARROW;
    }else if(stringKey == "KEY_LEFT_ARROW"){
      return KEY_LEFT_ARROW;
    }else if(stringKey == "KEY_RIGHT_ARROW"){
      return KEY_RIGHT_ARROW;
    }else if(stringKey == "KEY_BACKSPACE"){
      return KEY_BACKSPACE;
    }else if(stringKey == "KEY_TAB"){
      return KEY_TAB;
    }else if(stringKey == "KEY_RETURN"){
      return KEY_RETURN;
    }else if(stringKey == "KEY_ESC"){
      return KEY_ESC;
    }else if(stringKey == "KEY_INSERT"){
      return KEY_INSERT;
    }else if(stringKey == "KEY_DELETE"){
      return KEY_DELETE;
    }else if(stringKey == "KEY_PAGE_DOWN"){
      return KEY_PAGE_DOWN;
    }else if(stringKey == "KEY_HOME"){
      return KEY_HOME;
    }else if(stringKey == "KEY_ESC"){
      return KEY_ESC;
    }else if(stringKey == "KEY_END"){
      return KEY_END;
    }else if(stringKey == "KEY_CAPS_LOCK"){
      return KEY_CAPS_LOCK;
    }else if(stringKey == "KEY_F1"){
      return KEY_F1;
    }else if(stringKey == "KEY_F2"){
      return KEY_F2;
    }else if(stringKey == "KEY_F3"){
      return KEY_F3;
    }else if(stringKey == "KEY_F4"){
      return KEY_F4;
    }else if(stringKey == "KEY_F5"){
      return KEY_F5;
    }else if(stringKey == "KEY_F6"){
      return KEY_F6;
    }else if(stringKey == "KEY_F7"){
      return KEY_F7;
    }else if(stringKey == "KEY_F8"){
      return KEY_F8;
    }else if(stringKey == "KEY_F9"){
      return KEY_F9;
    }else if(stringKey == "KEY_F10"){
      return KEY_F10;
    }else if(stringKey == "KEY_F11"){
      return KEY_F11;
    }else if(stringKey == "KEY_F12"){
      return KEY_F12;
    }else if(stringKey == "KEY_F13"){
      return KEY_F13;
    }else if(stringKey == "KEY_F14"){
      return KEY_F14;
    }else if(stringKey == "KEY_F15"){
      return KEY_F15;
    }else if(stringKey == "KEY_F17"){
      return KEY_F17;
    }else if(stringKey == "KEY_F18"){
      return KEY_F18;
    }else if(stringKey == "KEY_F19"){
      return KEY_F19;
    }else if(stringKey == "KEY_F20"){
      return KEY_F20;
    }else if(stringKey == "KEY_F21"){
      return KEY_F21;
    }else if(stringKey == "KEY_F22"){
      return KEY_F22;
    }else if(stringKey == "KEY_F23"){
      return KEY_F23;
    }else if(stringKey == "KEY_F24"){
      return KEY_F24;
    }

    return (uint8_t*)stringKey[0];
}

