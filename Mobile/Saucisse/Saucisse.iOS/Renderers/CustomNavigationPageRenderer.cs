﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Saucisse.iOS.Renderers;
using Saucisse.Views;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomNavigationPage), typeof(CustomNavigationRenderer))]
namespace Saucisse.iOS.Renderers
{
	public class CustomNavigationRenderer : NavigationRenderer
	{
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			UINavigationBar.Appearance.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
			UINavigationBar.Appearance.ShadowImage = new UIImage();
			UINavigationBar.Appearance.BackgroundColor = UIColor.Clear;
			UINavigationBar.Appearance.TintColor = UIColor.White;
			UINavigationBar.Appearance.BarTintColor = UIColor.Clear;
			UINavigationBar.Appearance.Translucent = true;
			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
			{
				Font = UIFont.FromName("HelveticaNeue-Light", (nfloat)20f),
				TextColor = UIColor.White
			});
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}

			base.Dispose(disposing);
		}
	}
}