﻿using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Saucisse.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CircleButton : StackLayout
	{
		public static readonly BindableProperty TextProperty = BindableProperty.Create("Text", typeof(string), typeof(CircleButton), null);
		public static readonly BindableProperty IconProperty = BindableProperty.Create("Icon", typeof(string), typeof(CircleButton), null);
		public static readonly BindableProperty TapCommandProperty = BindableProperty.Create("TapCommand", typeof(ICommand), typeof(CircleButton), null);
		public static readonly BindableProperty TapCommandParameterProperty = BindableProperty.Create("TapCommandParameter", typeof(object), typeof(CircleButton), null);

		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public string Icon
		{
			get { return (string)GetValue(IconProperty); }
			set { SetValue(IconProperty, value); }
		}

		public ICommand TapCommand
		{
			get { return (ICommand)GetValue(TapCommandProperty); }
			set { SetValue(TapCommandProperty, value); }
		}

		public object TapCommandParameter
		{
			get { return GetValue(TapCommandParameterProperty); }
			set { SetValue(TapCommandParameterProperty, value); }
		}

		public CircleButton ()
		{
			InitializeComponent ();
		}
	}
}