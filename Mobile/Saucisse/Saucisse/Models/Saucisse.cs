﻿using System.Collections.Generic;

namespace Saucisse.Models
{
	public class Saucisse
	{
		public string Name { get; set; }

		public string Description { get; set; }

		public string Function { get; set; }

		public string Icon { get; set; }

		public List<SaucisseParameter> Parameters { get; set; }

		public Saucisse()
		{
			this.Parameters = new List<SaucisseParameter>();
		}

		public string GetCommand()
		{
			string parameters = $"{this.Function}?";

			foreach (var parameter in this.Parameters)
			{
				parameters += $"{parameter.Name}={parameter.Value}";
			}

			return parameters;
		}

	}
}
