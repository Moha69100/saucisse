﻿using Newtonsoft.Json;

namespace Saucisse.Models
{
	public class DeviceId
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("hardware")]
		public string Hardware { get; set; }

		[JsonProperty("connected")]
		public bool Connected { get; set; }
	}

	public class DeviceResult // {"return_value": 1, "id": "SC1416", "name": "SAUCISSE_1416", "hardware": "arduino", "connected": true}
	{
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("hardware")]
		public string Hardware { get; set; }

		[JsonProperty("connected")]
		public bool Connected { get; set; }

		[JsonProperty("return_value")]
		public bool IsSuccess { get; set; }
	}
}
