﻿namespace Saucisse.Models
{
	public class SaucisseParameter
	{
		public string Name { get; set; }

		public string Value { get; set; }
	}
}
