﻿using System.Collections.Generic;

namespace Saucisse.Models
{
	public class SaucisseConfigFile
	{
		public string MinFirmwareVersion { get; set; }

		public List<Saucisse> Saucisses { get; set; }

		public SaucisseConfigFile()
		{
			this.Saucisses = new List<Saucisse>();
		}
	}
}
