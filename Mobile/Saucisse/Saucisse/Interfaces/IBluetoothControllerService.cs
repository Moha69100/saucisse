﻿using Saucisse.Models;
using System.Threading.Tasks;

namespace Saucisse.Interfaces
{
    public interface IBluetoothControllerService
    {
		bool IsConnected { get; }

		Task<DeviceId> ConnectToDevice();

		Task<bool> SendCommand(string command);

		Task<string> WriteAsync(string command);

	}
}
