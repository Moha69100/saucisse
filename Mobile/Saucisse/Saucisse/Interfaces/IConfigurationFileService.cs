﻿using Saucisse.Models;
using System.Threading.Tasks;

namespace Saucisse.Interfaces
{
    public interface IConfigurationFileService
    {
		SaucisseConfigFile ConfigurationFile { get; }

		Task ReadConfigurationFile();
    }
}
