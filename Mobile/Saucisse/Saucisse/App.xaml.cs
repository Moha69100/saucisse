﻿using Prism;
using Prism.Ioc;
using Saucisse.ViewModels;
using Saucisse.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;
using Saucisse.Interfaces;
using Saucisse.Services;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Saucisse
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync("CustomNavigationPage/MainPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
		{
			containerRegistry.RegisterInstance<IConfigurationFileService>(new ConfigurationFileService());
			containerRegistry.RegisterInstance<IBluetoothControllerService>(new BluetoothControllerService());

			containerRegistry.RegisterForNavigation<CustomNavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage>();
			containerRegistry.RegisterForNavigation<SaucissesPage>();
		}
    }
}
