﻿using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Saucisse.Interfaces;
namespace Saucisse.ViewModels
{
    public class SaucissesPageViewModel : ViewModelBase
	{
		public SaucissesPageViewModel(INavigationService navigationService, IConfigurationFileService configurationFileService, 
			IPageDialogService dialogService, IBluetoothControllerService bluetoothControllerService)
			: base(navigationService)
		{
			this.Title = "Saucisses";
			this.ConfigurationFileService = configurationFileService;
			this.DialogService = dialogService;
			this.BluetoothControllerService = bluetoothControllerService;
			this.SaucisseCommand = new DelegateCommand<Models.Saucisse>(Saucisse);
		}

		public IConfigurationFileService ConfigurationFileService { get; }
		public IPageDialogService DialogService { get; }
		public IBluetoothControllerService BluetoothControllerService { get; }
		public DelegateCommand<Models.Saucisse> SaucisseCommand { get; set; }

		public override void OnNavigatedTo(NavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);
		}

		private async void Saucisse(Models.Saucisse saucisse)
		{
			var execute = await this.DialogService.DisplayAlertAsync($"Envoyer la saucisse : {saucisse.Name}", "", "Oui", "Non");

			if(execute && this.BluetoothControllerService.IsConnected)
			{
				await this.BluetoothControllerService.SendCommand(saucisse.GetCommand());
			}
			
		}
	}
}
