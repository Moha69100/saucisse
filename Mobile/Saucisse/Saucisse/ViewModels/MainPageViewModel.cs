﻿using Plugin.BLE;
using Plugin.BLE.Abstractions.Extensions;
using Prism.Commands;
using Prism.Navigation;
using Rg.Plugins.Popup.Extensions;
using Saucisse.Interfaces;
using Saucisse.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Saucisse.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
		public MainPageViewModel(INavigationService navigationService, IConfigurationFileService configurationFileService,
			IBluetoothControllerService bluetoothControllerService) 
            : base (navigationService)
        {
            this.Title = "Accueil";
			this.ConfigurationFileService = configurationFileService;
			this.BluetoothControllerService = bluetoothControllerService;
			this.SaucissesCommand = new DelegateCommand(Saucisses);
		}

		internal async void OnAppearing()
		{
			if(this.ConfigurationFileService.ConfigurationFile == null || !BluetoothControllerService.IsConnected)
				await Rg.Plugins.Popup.Services.PopupNavigation.Instance.PushAsync
					(new LoadingPopup(this.ConfigurationFileService, this.BluetoothControllerService));
		}

		public IConfigurationFileService ConfigurationFileService { get; }
		public IBluetoothControllerService BluetoothControllerService { get; }
		public DelegateCommand SaucissesCommand { get; set; }

		public override async void OnNavigatedTo(NavigationParameters parameters)
		{
			base.OnNavigatedTo(parameters);

			
			var adapter = CrossBluetoothLE.Current.Adapter;
			//adapter.DeviceDiscovered += Adapter_DeviceDiscovered;
			//adapter.DeviceConnected += Adapter_DeviceConnected;
			//var deviceGuid = Guid.Parse("186719ff-c5b8-0250-b36e-a2ae8a6af7f6");
			//var device = await adapter.ConnectToKnownDeviceAsync(deviceGuid);
			 
			/*await this.ConfigurationFileService.ReadConfigurationFile();*/
		}

		const string RETURN_LINE = "\r\n";

		private async void Adapter_DeviceConnected(object sender, Plugin.BLE.Abstractions.EventArgs.DeviceEventArgs e)
		{
			var service = await e.Device.GetServiceAsync(0x0FFE0.UuidFromPartial());
			var characteristic = await service.GetCharacteristicAsync(0x0FFE1.UuidFromPartial());

			characteristic.ValueUpdated += Characteristic_ValueUpdated;
			await characteristic.StartUpdatesAsync();

			var data = System.Text.Encoding.ASCII.GetBytes("runCommand?command=KEY_WIN|KEY_DOWN|KEY_UP|r" + RETURN_LINE);

			var stopWatch = new Stopwatch();
			stopWatch.Start();
			await characteristic.WriteAsync(data);
			stopWatch.Stop();
			Debug.WriteLine("Stopwatch : " + stopWatch.Elapsed);
			await characteristic.WriteAsync(data);
			await characteristic.WriteAsync(data);
			
		}

		List<byte> bytes = new List<byte>();
		private static readonly Object obj = new Object();


		private void Characteristic_ValueUpdated(object sender, Plugin.BLE.Abstractions.EventArgs.CharacteristicUpdatedEventArgs e)
		{
			lock (obj)
			{
				bytes.AddRange(e.Characteristic.Value);

				if (e.Characteristic.StringValue.Contains(RETURN_LINE)) //bytes.Count > 2 && bytes[bytes.Count - 1] == 0x0a && bytes[bytes.Count - 2] == 0x0d)
				{
					var text = new string(System.Text.Encoding.UTF8.GetChars(bytes.ToArray()));
					bytes.Clear();
					Debug.WriteLine("Characteristic_ValueUpdated : " + text);
				}
			}
		}

		private async void Adapter_DeviceDiscovered(object sender, Plugin.BLE.Abstractions.EventArgs.DeviceEventArgs e)
		{
			Debug.WriteLine(e.Device.Name);

			if(e.Device.Name == "SH-HC-08")
			{
				var adapter = CrossBluetoothLE.Current.Adapter;
				var guid = e.Device.Id.ToString();

				await adapter.ConnectToDeviceAsync(e.Device);
				
			}
		}

		private async void Saucisses()
		{
			await this.NavigationService.NavigateAsync("SaucissesPage");
		}
	}
}
