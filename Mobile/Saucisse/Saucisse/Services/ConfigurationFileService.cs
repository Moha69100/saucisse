﻿using Newtonsoft.Json;
using Saucisse.Interfaces;
using Saucisse.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Saucisse.Services
{
	public class ConfigurationFileService : IConfigurationFileService
	{
		private const string Url = "https://bitbucket.org/Moha69100/saucisse/raw/master/SaucisseConfigFile.json?date={0}";

		public SaucisseConfigFile ConfigurationFile { get; private set; }

		public async Task ReadConfigurationFile()
		{
			var client = new HttpClient();
			var json = await client.GetStringAsync(string.Format(Url, DateTime.Now.Ticks.ToString()));
			this.ConfigurationFile = JsonConvert.DeserializeObject<SaucisseConfigFile>(json);
		}
	}
}
