﻿using Newtonsoft.Json;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using Plugin.BLE.Abstractions.Extensions;
using Saucisse.Interfaces;
using Saucisse.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Saucisse.Services
{
	public class BluetoothControllerService : IBluetoothControllerService
	{
		const string RETURN_LINE = "\r\n";

		public bool IsConnected { get; private set; }

		private IBluetoothLE BluetoothLE = CrossBluetoothLE.Current;
		private IAdapter Adapter = CrossBluetoothLE.Current.Adapter;
		private IDevice Device;
		private IService Service;
		private ICharacteristic Characteristic;

		public async Task<DeviceId> ConnectToDevice()
		{
			var deviceGuid = Guid.Parse("186719ff-c5b8-0250-b36e-a2ae8a6af7f6");
			Device = await Adapter.ConnectToKnownDeviceAsync(deviceGuid);

			if (Device == null)
				return null;

			Service = await Device.GetServiceAsync(0x0FFE0.UuidFromPartial());

			if (Service == null)
				return null;

			Characteristic = await Service.GetCharacteristicAsync(0x0FFE1.UuidFromPartial());
			
			if (Characteristic == null)
				return null;

			Characteristic.ValueUpdated += Characteristic_ValueUpdated;
			await Characteristic.StartUpdatesAsync();
			
			var json = await this.WriteAsync("id");

			var jsonObject = JsonConvert.DeserializeObject<DeviceId>(json);

			this.IsConnected = jsonObject != null;

			return jsonObject;
		}


		TaskCompletionSource<string> taskCompletionSource;

		public async Task<string> WriteAsync(string command)
		{
			taskCompletionSource = new TaskCompletionSource<string>(); 

			var data = Encoding.ASCII.GetBytes(command + RETURN_LINE);

			var result = await Characteristic.WriteAsync(data);

			var returnValue = await taskCompletionSource.Task;
			
			return returnValue;
		}

		List<byte> bytes = new List<byte>();
		private static readonly Object obj = new Object();
		private void Characteristic_ValueUpdated(object sender, CharacteristicUpdatedEventArgs e)
		{
			lock (obj)
			{
				bytes.AddRange(e.Characteristic.Value);

				if (e.Characteristic.StringValue.Contains(RETURN_LINE)) 	{
					var text = new string(System.Text.Encoding.UTF8.GetChars(bytes.ToArray()));
					bytes.Clear();
					taskCompletionSource.TrySetResult(text);
				}
			}
		}

		public async Task<bool> SendCommand(string command)
		{
			var json = await this.WriteAsync(command);

			var jsonObject = JsonConvert.DeserializeObject<DeviceResult>(json);
			
			return jsonObject?.IsSuccess ?? false;
		}
	}
}
