﻿using Saucisse.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Saucisse.Views
{
	public partial class MainPage : ContentPage
	{
		public MainPage ()
		{
			InitializeComponent ();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if(this.BindingContext is MainPageViewModel mainPageViewModel)
			{
				mainPageViewModel.OnAppearing();
			}
		}
	}
}