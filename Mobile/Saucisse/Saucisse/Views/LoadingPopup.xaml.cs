﻿using Saucisse.Interfaces;
using System.Threading.Tasks;
using Xamarin.Forms.Xaml;

namespace Saucisse.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoadingPopup
	{
		private readonly IConfigurationFileService configurationFileService;
		private readonly IBluetoothControllerService bluetoothControllerService;

		public LoadingPopup (IConfigurationFileService configurationFileService, IBluetoothControllerService bluetoothControllerService)
		{
			InitializeComponent ();
			this.configurationFileService = configurationFileService;
			this.bluetoothControllerService = bluetoothControllerService;
		}

		protected async override void OnAppearing()
		{
			base.OnAppearing();

			lblText.Text = "Load config file";
			await this.configurationFileService.ReadConfigurationFile();

			lblText.Text = "Connect to device";
			var deviceId = await bluetoothControllerService.ConnectToDevice();

			lblText.Text = "Connected to " + deviceId.Id;

			await Task.Delay(2500);

			await Rg.Plugins.Popup.Services.PopupNavigation.Instance.PopAsync();
		}
		
	}
}